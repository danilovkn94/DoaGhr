package ru.sut.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class LhsCompany {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    private String name;

    @NotNull
    private String address;

    @ManyToOne
    @JoinColumn(name = "MPA_COMPANY_ID")
    private MpaCompany mpaCompany;

    public LhsCompany() {
    }

    public LhsCompany(String name, String address, MpaCompany mpaCompany) {
        this.name = name;
        this.address = address;
        this.mpaCompany = mpaCompany;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public MpaCompany getMpaCompany() {
        return mpaCompany;
    }

    public void setMpaCompany(MpaCompany mpaCompany) {
        this.mpaCompany = mpaCompany;
    }
}
