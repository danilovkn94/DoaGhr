package ru.sut.controlla;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.sut.model.LhsCompany;
import ru.sut.model.NetworkAddress;
import ru.sut.model.User;
import ru.sut.repository.LhsCompanyRepository;
import ru.sut.repository.NetworkAddressRepository;

@Controller
public class NetAddressControlla {

    private final NetworkAddressRepository networkAddressRepository;
    private final LhsCompanyRepository lhsCompanyRepository;

    @Autowired
    public NetAddressControlla(NetworkAddressRepository networkAddressRepository,
                               LhsCompanyRepository lhsCompanyRepository)
    {
        this.networkAddressRepository = networkAddressRepository;
        this.lhsCompanyRepository = lhsCompanyRepository;
    }

    @RequestMapping(value = "/networkAddresses", method = RequestMethod.GET)
    public String networkAddresses(Model model){

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        model.addAttribute("netAddrs",
                networkAddressRepository.findByLhsCompany_MpaCompany_Id(user.getMpaCompany().getId()));
        model.addAttribute("lhs", lhsCompanyRepository.findByMpaCompanyId(user.getMpaCompany().getId()));

        return "network_addresses";
    }

    @RequestMapping(value = "/saveNetworkAddress", method = RequestMethod.POST)
    public String saveNetworkAddress(@RequestParam("host") String host,
                                     @RequestParam("port") int port,
                                     @RequestParam("lhsCompany")LhsCompany lhsCompany)
    {
        NetworkAddress networkAddress = new NetworkAddress(host, port, lhsCompany);

        networkAddressRepository.saveAndFlush(networkAddress);

        return "redirect:/networkAddresses";
    }

    @RequestMapping(value = "/deleteNetworkAddress", method = RequestMethod.POST)
    public String deleteNetworkAddress(@RequestParam("id") int id){

        networkAddressRepository.delete(id);

        return "redirect:/networkAddresses";
    }
}
