package ru.sut.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.sut.model.MpaCompany;

public interface MpaCompanyRepository extends JpaRepository<MpaCompany, Integer>{
}
