package ru.sut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@SpringBootApplication
public class DoaGhrApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoaGhrApplication.class, args);
	}
}
