package ru.sut.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.sut.model.LhsCompany;

import java.util.List;

public interface LhsCompanyRepository extends JpaRepository<LhsCompany, Integer>{

    List<LhsCompany> findByMpaCompanyId(int mpaCompanyId);
}
