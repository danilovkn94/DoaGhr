package ru.sut.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.sut.model.NetworkAddress;

import java.util.List;

public interface NetworkAddressRepository extends JpaRepository<NetworkAddress, Integer>{

    List<NetworkAddress> findByLhsCompany_MpaCompany_Id(int id);
}
