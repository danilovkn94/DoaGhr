package ru.sut.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class NetworkAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    private String host;

    @NotNull
    private int port;

    @ManyToOne
    @JoinColumn(name = "LHS_COMPANY_ID")
    private LhsCompany lhsCompany;

    public NetworkAddress() {
    }

    public NetworkAddress(String host, int port, LhsCompany lhsCompany) {
        this.host = host;
        this.port = port;
        this.lhsCompany = lhsCompany;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public LhsCompany getLhsCompany() {
        return lhsCompany;
    }

    public void setLhsCompany(LhsCompany lhsCompany) {
        this.lhsCompany = lhsCompany;
    }

    public String getPreffix() {
        return lhsCompany.getMpaCompany().getId() + "." + lhsCompany.getId() + "." + id;
    }
}
