package ru.sut.controlla;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sut.model.NetworkAddress;
import ru.sut.repository.NetworkAddressRepository;

@RequestMapping(value = "/rest")
@RestController
public class RestControlla {

    private final NetworkAddressRepository networkAddressRepository;

    @Autowired
    public RestControlla(NetworkAddressRepository networkAddressRepository) {
        this.networkAddressRepository = networkAddressRepository;
    }


    @GetMapping("/{preffix}/")
    public NetworkAddress getInfo(@PathVariable String preffix){

        String[] s = preffix.split("\\.");

        return networkAddressRepository.findOne(Integer.parseInt(s[s.length - 1]));
    }
}
