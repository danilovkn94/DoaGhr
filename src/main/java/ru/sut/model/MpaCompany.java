package ru.sut.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class MpaCompany {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    private String companyName;

    @JsonIgnore
    @OneToMany(mappedBy = "mpaCompany")
    private List<User> user;

    @JsonIgnore
    @OneToMany(mappedBy = "mpaCompany")
    private List<LhsCompany> lhsCompany;

    public MpaCompany() {
    }

    public List<User> getUser() {
        return user;
    }

    public void setUser(List<User> user) {
        this.user = user;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<LhsCompany> getLhsCompany() {
        return lhsCompany;
    }

    public void setLhsCompany(List<LhsCompany> lhsCompany) {
        this.lhsCompany = lhsCompany;
    }
}
