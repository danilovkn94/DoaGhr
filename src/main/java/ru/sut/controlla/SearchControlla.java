package ru.sut.controlla;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.sut.model.NetworkAddress;
import ru.sut.repository.NetworkAddressRepository;

@Controller
public class SearchControlla {

    private final NetworkAddressRepository networkAddressRepository;

    @Autowired
    public SearchControlla(NetworkAddressRepository networkAddressRepository) {
        this.networkAddressRepository = networkAddressRepository;
    }


    @RequestMapping(value = "/search")
    public String search(@RequestParam("identificator") String identificator){
        String[] i = identificator.split("/");
        String[] p = i[0].split("\\.");

        NetworkAddress networkAddress = networkAddressRepository.findOne(Integer.parseInt(p[p.length - 1]));

        String url = "http://" + networkAddress.getHost() + ":" + networkAddress.getPort()
                + "/search?identificator=" + identificator;

        return "redirect:" + url;
    }
}
