package ru.sut.controlla;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.sut.model.LhsCompany;
import ru.sut.model.User;
import ru.sut.repository.LhsCompanyRepository;

@Controller
public class LhsControlla {

    private final LhsCompanyRepository lhsCompanyRepository;

    @Autowired
    public LhsControlla(LhsCompanyRepository lhsCompanyRepository) {
        this.lhsCompanyRepository = lhsCompanyRepository;
    }

    @RequestMapping(value = "/lhs", method = RequestMethod.GET)
    public String lhs(Model model){

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        model.addAttribute("lhs", lhsCompanyRepository.findByMpaCompanyId(user.getMpaCompany().getId()));

        return "lhs";
    }

    @RequestMapping(value = "/saveLhs", method = RequestMethod.POST)
    public String saveConsignment(@RequestParam("name") String name,
                                  @RequestParam("address") String address)
    {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        LhsCompany lhsCompany = new LhsCompany(name, address, user.getMpaCompany());
        lhsCompanyRepository.saveAndFlush(lhsCompany);

        return "redirect:/lhs";
    }

    @RequestMapping(value = "deleteLhs", method = RequestMethod.POST)
    public String deleteConsignment(@RequestParam("id") int id){

        lhsCompanyRepository.delete(id);

        return "redirect:/lhs";
    }


}
